<?php

namespace Bilot\Test\Model;

use Bilot\Test\Api\TestInterface;

class TestRepository implements TestInterface {

    /**
     * @var \Magento\Framework\Filesystem\File\WriteFactory
     */
    protected $writeFactory;
    
    /**
     * @var \Magento\Framework\Filesystem\File\Write
     */
    protected $write;

    /**
     * @param \Magento\Framework\View\Design\Theme\Customization\FileServiceFactory $fileServiceFactory
     */
    public function __construct(\Magento\Framework\Filesystem\File\WriteFactory $writeFactory) {
        $this->writeFactory = $writeFactory;
        $this->write = $this->writeFactory->create("c:\\temp\\test.txt", \Magento\Framework\Filesystem\DriverPool::FILE, "w+");
    }

    /**
     * {@inheritdoc}
     */
    public function get($name = "Unknown") {
        $msg = "Hello, " . $name;
        $this->write->write($msg);
        return $msg;
    }

}

?>