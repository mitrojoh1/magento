<?php

namespace Bilot\Test\Api;

/**
 * @api
 */
interface TestInterface
{
    /**
     * Get hello world
     *
     * @param string $name
     * @return string Hello <name>
     */
     public function get($name = "Unknown");
}	

?>