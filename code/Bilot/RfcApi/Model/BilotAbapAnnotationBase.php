<?php

namespace Bilot\RfcApi\Model;


use Bilot\IdocReceiver\Model\Util\Console;

class BilotAbapAnnotationBase
{

    /**
     * @var array
     */
    protected $data;

    /**
     * @var \ReflectionProperty
     */
    protected $property;


    /**
     * BilotAbapAnnotationBase constructor.
     * @param \ReflectionProperty $property
     * @param array $data
     */
    public function __construct(\ReflectionProperty $property, $data)
    {
        $this->property = $property;
        $this->property = $property;
        $this->data = $data;
        if (is_null($this->data)) {
            $this->data = array();
        }
    }

    /**
     * @return bool
     */
    public function isDefined() {
        return !empty($this->data);
    }

    /**
     * @return \ReflectionProperty
     */
    public function getField()
    {
        return $this->property;
    }

    /**
     * @param $string1
     * @param $string2
     * @return bool
     */
    protected function matches($string1, $string2) {
        return strcmp($string1, $string2) === 0;
    }

}