<?php

namespace Bilot\RfcApi\Model;


abstract class RfcBeanBase
{

    /**
     * @var \Bilot\RfcApi\Model\Service\RfcService
     */
    protected $rfcService;


    /**
     * RfcBeanBase constructor.
     * @param Service\RfcService $rfcService
     */
    public function __construct(\Bilot\RfcApi\Model\Service\RfcService $rfcService)
    {
        $this->rfcService = $rfcService;
    }

    public abstract function getFunctionModuleName();

}