<?php

namespace Bilot\RfcApi\Model\Service;


use Bilot\IdocReceiver\Model\Util\Console;
use Bilot\RfcApi\Api\BilotAbapAnnotationInterface;
use Bilot\RfcApi\Helper\AnnotationParser;
use Magento\Framework\HTTP\ZendClient;

class RfcService
{
    const DATA = "DATA";

    const PARAM_INTERFACE = "BSM_INTERFACE";
    const PARAM_NAME = "BSM_NAME";

    const INTERFACE_FUNCTION = "FUNCTION";
    const INTERFACE_CLASS = "CLASS";

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $httpClientFactory;

    /**
     * @var \Bilot\RfcApi\Model\RfcConfig $config
     */
    protected $config;

    public function __construct(
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Bilot\RfcApi\Model\RfcConfig $config)
    {
        $this->httpClientFactory = $httpClientFactory;
        $this->config = $config;
    }

    /**
     * RFC call to ERP. Provided RfcBean provides data and metadata for the call.
     * 
     * @param \Bilot\RfcApi\Model\RfcBeanBase $rfcBean
     */
    public function execute($rfcBean)
    {
        $json = $this->populateParams($rfcBean);

        Console::log("Request: " . $json);

        $client = $this->getClient()->setRawData($json);
        $responseBody = $client->request()->getBody();

        $this->parseReturn($rfcBean, $responseBody);
    }

    /**
     * @return ZendClient
     */
    protected function getClient()
    {
        $client = $this->httpClientFactory->create();
        $client->setMethod(ZendClient::POST);
        $client->setAuth($this->config->getRfcUser(), $this->config->getRfcUserPassword());
        $client->setUri($this->config->getRfcApi());
        return $client;
    }

    /**
     * @param \Bilot\RfcApi\Model\RfcBeanBase $rfcBean
     * @return string json string
     */
    protected function populateParams($rfcBean)
    {
        $reflectionClass = new \ReflectionClass($rfcBean);
        $properties = $reflectionClass->getProperties();

        $data = array();

        $this->fillFunctionModule($rfcBean, $data);

        foreach ($properties as $property) {

            // allow access to private members as well
            $property->setAccessible(true);

            // read annotation
            $annotation = AnnotationParser::readBilotAbapBackendAnnotation($property);
            if ($annotation->isDefined()) {

                if ($annotation->isSimpleParameter()) {

                    // simple parameter
                    $this->fillSimpleParam($rfcBean, $annotation, $data);

                } else if ($annotation->isStructureParameter()) {

                    // structure parameter
                    $data[$annotation->getNameParameter()] = $this->handleStructuredAttribute($property->getValue($rfcBean), $annotation);

                } else if ($annotation->isTableParameter()) {

                    // table parameters
                    $table = $property->getValue($rfcBean);

                    $values = array();
                    if (!is_null($table) && !empty($table)) {

                        foreach ($table as $row) {
                            array_push($values, $this->handleStructuredAttribute($row, $annotation));
                        }
                    }
                    $data[$annotation->getNameParameter()] = $values;

                }
            }
        }

        // string representation of json to be used in RFC Api call
        return json_encode([self::DATA => $data], JSON_UNESCAPED_SLASHES);
    }

    /**
     * @param \Bilot\RfcApi\Model\RfcBeanBase $rfcBean
     * @param string $json
     */
    protected function parseReturn($rfcBean, $json)
    {

        if (is_null($json) || empty($json)) {

            Console::log("Empty response - existing.");
            // nothing to parse
            return;
        }

        $data = json_decode($json)->{self::DATA};

        $reflectionClass = new \ReflectionClass($rfcBean);
        $properties = $reflectionClass->getProperties();

        foreach ($properties as $property) {

            // allow access to private members as well
            $property->setAccessible(true);

            // read annotation
            $annotation = AnnotationParser::readBilotAbapBackendAnnotation($property);

            if ($annotation->isDefined()) {

                if ($annotation->isExportParameter() || $annotation->isTableParameter() || $annotation->isChangingParameter()) {

                    if ($annotation->isSimpleParameter()) {

                        // simple parameter
                        $property->setValue($rfcBean, $data->{$annotation->getAbapFieldName()});

                    } else if ($annotation->isStructureParameter()) {

                        // structure parameters
                        $this->parseStructureParam($rfcBean, $property, $annotation, $data);

                    } else if ($annotation->isTableParameter()) {

                        // table parameters
                        $this->parseTableParam($rfcBean, $annotation, $data, $property);

                    }

                }

            }

        }

    }

    /**
     * @param mixed $value
     * @param BilotAbapAnnotationInterface $annotation
     * @return array | mixed
     */
    protected function handleStructuredAttribute($value, $annotation)
    {
        $return = null;

        if (!is_null($value)) {

            if ((is_object($value)) || !is_null($annotation->getStructureClass())) {

                // class based structured attribute
                $attrReflectionClass = new \ReflectionClass($value);
                $attrProperties = $attrReflectionClass->getProperties();

                $values = array();

                foreach ($attrProperties as $attrProperty) {

                    // allow access to private members as well
                    $attrProperty->setAccessible(true);

                    // read annotation
                    $attrAnnotation = AnnotationParser::readBilotAbapAttributeAnnotation($attrProperty);

                    if ($attrAnnotation->isDefined()) {
                        $this->fillSimpleParam($value, $attrAnnotation, $values);
                    }
                }

                $return = $values;

            } else if (is_array($value)) {

                // simple array
                $return = $value;
            }
        }

        return $return;
    }

    /**
     * @param $rfcBean
     * @param $property
     * @param $annotation
     * @param $data
     */
    protected function parseStructureParam($rfcBean, $property, $annotation, &$data)
    {
        $value = $property->getValue($rfcBean);

        // structure parameter
        if ((is_object($value)) || !is_null($annotation->getStructureClass())) {

            // class based structure
            if (is_null($value) && !is_null($annotation->getStructureClass())) {

                // instantiate attribute value based on structure class
                $className = $annotation->getStructureClass();
                $instance = new $className();
                $property->setValue($rfcBean, $instance);
            }

            $value = $property->getValue($rfcBean);
            $attrReflectionClass = new \ReflectionClass($value);
            $attrProperties = $attrReflectionClass->getProperties();

            foreach ($attrProperties as $attrProperty) {

                // allow access to private members as well
                $attrProperty->setAccessible(true);

                // read annotation
                $attrAnnotation = AnnotationParser::readBilotAbapAttributeAnnotation($attrProperty);

                if ($attrAnnotation->isDefined()) {

                    // set structure field value
                    $attrProperty->setValue($value, $data->{$annotation->getAbapFieldName()}->{$attrAnnotation->getAbapFieldName()});
                }
            }

        } else if (is_array($value)) {

            // simple array
            $property->setValue($rfcBean, $value);
        }
    }

    /**
     * @param $rfcBean
     * @param $annotation
     * @param $data
     * @param $property
     */
    protected function parseTableParam($rfcBean, $annotation, $data, $property)
    {

        // table parameter
        $className = $annotation->getStructureClass();

        $values = array();

        $currentTable = $data->{$annotation->getAbapFieldName()};

        if (!is_null($currentTable) && !empty($currentTable)) {

            foreach ($currentTable as $row) {

                if (empty($className)) {

                    // simple array
                    array_push($values, $row);

                } else {

                    // class based structure
                    $instance = new $className();

                    $attrReflectionClass = new \ReflectionClass($instance);
                    $attrProperties = $attrReflectionClass->getProperties();

                    foreach ($attrProperties as $attrProperty) {

                        // allow access to private members as well
                        $attrProperty->setAccessible(true);

                        // read annotation
                        $attrAnnotation = AnnotationParser::readBilotAbapAttributeAnnotation($attrProperty);

                        if ($attrAnnotation->isDefined()) {

                            // set structure field value
                            $attrProperty->setValue($instance, $row->{$attrAnnotation->getAbapFieldName()});
                        }
                    }

                    array_push($values, $instance);
                }

            }
        }

        $property->setValue($rfcBean, $values);
    }

    /**
     * @param \Bilot\RfcApi\Model\RfcBeanBase $rfcBean
     * @param array $data
     */
    protected function fillFunctionModule($rfcBean, &$data)
    {
        $data[self::PARAM_NAME] = $rfcBean->getFunctionModuleName();
        $data[self::PARAM_INTERFACE] = self::INTERFACE_FUNCTION;
    }

    /**
     * @param object $object
     * @param \Bilot\RfcApi\Api\BilotAbapBackendAnnotationInterface $annotation
     * @param array $data
     */
    protected function fillSimpleParam($object, $annotation, &$data)
    {
        $value = (string)$annotation->getField()->getValue($object);
        if (!is_null($value)) {
            $data[$annotation->getAbapFieldName()] = $value;
        }
    }

}