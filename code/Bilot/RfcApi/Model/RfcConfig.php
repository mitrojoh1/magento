<?php

namespace Bilot\RfcApi\Model;

class RfcConfig
{

    /**
     * Configuration path: RFC api url
     */
    const BSM_URL = "rfcconnection/general/url";

    /**
     * Configuration path: RFC api user
     */
    const BSM_USER = "rfcconnection/general/username";

    /**
     * Configuration path: RFC api password
     */
    const BSM_PASSWORD = "rfcconnection/general/password";


    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get RFC api url
     *
     * @return string RFC api
     */
    public function getRfcApi()
    {
        return $this->scopeConfig->getValue(self::BSM_URL);
    }

    /**
     * @return string username
     */
    public function getRfcUser()
    {
        return $this->scopeConfig->getValue(self::BSM_USER);
    }

    /**
     * @return string password
     */
    public function getRfcUserPassword()
    {
        return $this->scopeConfig->getValue(self::BSM_PASSWORD);
    }

}

?>