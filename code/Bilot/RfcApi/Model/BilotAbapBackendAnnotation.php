<?php

namespace Bilot\RfcApi\Model;


use Bilot\RfcApi\Api\BilotAbapBackendAnnotationInterface;

class BilotAbapBackendAnnotation extends BilotAbapAnnotationBase implements BilotAbapBackendAnnotationInterface
{

    /**
     * BilotAbapBackendAnnotation constructor.
     * @param \ReflectionProperty $property
     * @param array $data
     */
    public function __construct($property, $data)
    {
        parent::__construct($property, $data);
    }



    /**
     * @return bool
     */
    public function isSimpleParameter() {
        return $this->matches(self::TYPE_SIMPLE, $this->getTypeParameter());
    }

    /**
     * @return bool
     */
    public function isStructureParameter() {
        return $this->matches(self::TYPE_STRUCTURE, $this->getTypeParameter());
    }

    /**
     * @return bool
     */
    public function isTableParameter() {
        return $this->matches(self::TYPE_TABLE, $this->getTypeParameter());
    }

    /**
     * @return bool
     */
    public function isImportParameter() {
        return $this->matches(self::DIRECTION_IMPORT, $this->getDirectionParameter());
    }

    /**
     * @return bool
     */
    public function isExportParameter() {
        return $this->matches(self::DIRECTION_EXPORT, $this->getDirectionParameter());
    }

    /**
     * @return bool
     */
    public function isChangingParameter() {
        return $this->matches(self::DIRECTION_CHANGING, $this->getDirectionParameter()) === 0;
    }


    /**
     * @return string
     */
    private function getTypeParameter() {
        return $this->data[self::ATTR_TYPE];
    }

    /**
     * @return string
     */
    private function getDirectionParameter() {
        return $this->data[self::ATTR_DIRECTION];
    }

    /**
     * @return string
     */
    public function getNameParameter() {
        return $this->data[self::ATTR_NAME];
    }

    /**
     * @return string
     */
    public function getStructureClass()
    {
        return $this->data[self::ATTR_STRUCT_CLASS];
    }

    /**
     * @inheritDoc
     */
    public function getAbapFieldName()
    {
        return $this->getNameParameter();
    }

}