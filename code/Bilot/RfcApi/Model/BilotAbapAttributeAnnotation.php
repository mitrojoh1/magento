<?php
/**
 * Created by PhpStorm.
 * User: mitrojoh1
 * Date: 15.11.2016
 * Time: 14:40
 */

namespace Bilot\RfcApi\Model;


use Bilot\RfcApi\Api\BilotAbapAttributeAnnotationInterface;

class BilotAbapAttributeAnnotation extends BilotAbapAnnotationBase implements BilotAbapAttributeAnnotationInterface
{

    /**
     * BilotAbapAttributeAnnotation constructor.
     * @param \ReflectionProperty $property
     * @param array $data
     */
    public function __construct(\ReflectionProperty $property, $data)
    {
        parent::__construct($property, $data);
    }

    /**
     * @inheritDoc
     */
    public function getFieldName()
    {
        return $this->data[self::ATTR_FIELD];
    }

    /**
     * @inheritDoc
     */
    public function getAbapFieldName()
    {
        return $this->getFieldName();
    }


}