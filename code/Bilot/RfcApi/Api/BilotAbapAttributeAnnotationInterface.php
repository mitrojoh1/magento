<?php

namespace Bilot\RfcApi\Api;


/**
 * Interface describing BilotAbapBackendAnnotationInterface
 *
 * @bilotabapattribute [field=FIELD_NAME]
 *
 * @package Bilot\RfcApi\Api
 */
interface BilotAbapAttributeAnnotationInterface extends BilotAbapAnnotationInterface
{
    const ATTR_FIELD = "field";



    /**
     * @return string
     */
    public function getFieldName();

}