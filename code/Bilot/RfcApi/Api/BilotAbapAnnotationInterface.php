<?php

namespace Bilot\RfcApi\Api;


interface BilotAbapAnnotationInterface
{

    /**
     * @return bool
     */
    public function isDefined();

    /**
     * @return \ReflectionProperty
     */
    public function getField();

    /**
     * @return string
     */
    public function getAbapFieldName();

}