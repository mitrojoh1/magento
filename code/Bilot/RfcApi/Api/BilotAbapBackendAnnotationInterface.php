<?php

namespace Bilot\RfcApi\Api;

/**
 * Interface describing BilotAbapBackendAnnotationInterface
 *
 * @bilotabapbackend [type=TABLE, direction=CHANGING, name=IT_TEST, structureClass=/Vendor/NameSpace/Class]
 *
 * @package Bilot\RfcApi\Api
 */
interface BilotAbapBackendAnnotationInterface extends BilotAbapAnnotationInterface
{
    const ATTR_TYPE = "type";
    const ATTR_DIRECTION = "direction";
    const ATTR_NAME = "name";
    const ATTR_STRUCT_CLASS = "structureClass";

    const TYPE_TABLE = "TABLE";
    const TYPE_STRUCTURE = "STRUCTURE";
    const TYPE_SIMPLE = "SIMPLE";

    const DIRECTION_IMPORT = "IMPORTING";
    const DIRECTION_EXPORT = "EXPORTING";
    const DIRECTION_CHANGING = "CHANGING";


    /**
     * @return bool
     */
    public function isSimpleParameter();

    /**
     * @return bool
     */
    public function isStructureParameter();

    /**
     * @return bool
     */
    public function isTableParameter();

    /**
     * @return bool
     */
    public function isImportParameter();

    /**
     * @return bool
     */
    public function isExportParameter();

    /**
     * @return bool
     */
    public function isChangingParameter();

    /**
     * @return string
     */
    public function getNameParameter();

    /**
     * @return string
     */
    public function getStructureClass();


}