<?php

namespace Bilot\RfcApi\Helper;


use Bilot\IdocReceiver\Model\Util\Console;
use Bilot\RfcApi\Api\BilotAbapAttributeAnnotationInterface;
use Bilot\RfcApi\Api\BilotAbapBackendAnnotationInterface;
use Bilot\RfcApi\Model\BilotAbapAttributeAnnotation;
use Bilot\RfcApi\Model\BilotAbapBackendAnnotation;

class AnnotationParser
{

    /**
     * Regex pattern for parsing PhpDoc comments blocks for <tt>bilotabapbackend</tt> annotations.
     */
    const PATTERN_BACKEND = "#@[bilotabapbackend]+\s*\[([(a-zA-Z0-9)].*)\]#";

    const PATTERN_ATTRIBUTE = "#@[bilotabapattribute]+\s*\[([(a-zA-Z0-9)].*)\]#";

    /**
     * Parse PhpDoc comment block for bilot annotation.
     *
     * @param \ReflectionProperty $property
     * @return BilotAbapBackendAnnotationInterface
     */
    public static function readBilotAbapBackendAnnotation(\ReflectionProperty $property) {
        preg_match_all(self::PATTERN_BACKEND, $property->getDocComment(), $annotation, PREG_PATTERN_ORDER);
        $config = self::parseAnnotationParams($annotation);
        return new BilotAbapBackendAnnotation($property, $config);
    }

    /**
     * Parse PhpDoc comment block for bilot annotation.
     *
     * @param \ReflectionProperty $property
     * @return BilotAbapAttributeAnnotationInterface
     */
    public static function readBilotAbapAttributeAnnotation(\ReflectionProperty $property) {
        preg_match_all(self::PATTERN_ATTRIBUTE, $property->getDocComment(), $annotation, PREG_PATTERN_ORDER);
        $config = self::parseAnnotationParams($annotation);
        return new BilotAbapAttributeAnnotation($property, $config);
    }

    /**
     * @param array $annotation
     * @return array|null
     */
    private static function parseAnnotationParams($annotation) {
        $config = null;
        if (count($annotation) > 1 && count($annotation[1]) > 0) {
            $config = array();
            $params = explode(",", $annotation[1][0]);
            foreach ($params as $param) {
                $keyValues = explode("=", trim($param));
                $config[$keyValues[0]] = $keyValues[1];
            }
        }
        return $config;
    }

}