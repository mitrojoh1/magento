<?php

namespace Bilot\IdocReceiver\Api;

interface IdocStatusRepositoryInterface {

    /**
     * @param \Bilot\IdocReceiver\Api\Data\IdocStatusInterfac $idocStatus
     * @return \Bilot\IdocReceiver\Api\Data\IdocStatusInterfac
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Bilot\IdocReceiver\Api\Data\IdocStatusInterface $idocStatus);

    /**
     * @return \Bilot\IdocReceiver\Api\Data\IdocStatusInterface[]
     */
    public function getNonProcessedEntries();

    /**
     * @param \Bilot\IdocReceiver\Api\Data\IdocStatusInterface[] $statuses
     */
    public function updateProcessingStatusToProcessed($statuses);

}

?>