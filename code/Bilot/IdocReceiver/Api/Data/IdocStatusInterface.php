<?php

namespace Bilot\IdocReceiver\Api\Data;

interface IdocStatusInterface extends \Magento\Framework\Api\CustomAttributesDataInterface {

    const IDOC_NUMBER = "idoc_number";

    const IDOC_TYPE = "idoc_type";

    const MESSAGE_TYPE = "message_type";

    const STATUS = "status";

    const STATUS_TEXT = "status_text";

    const PROCESSING_STATUS = "processing_status";


    /**
     * @return string
     */
    public function getIdocNumber();

    /**
     * @param string $idocNumber
     * @return void
     */
    public function setIdocNumber($idocNumber);

    /**
     * @return string
     */
    public function getIdocType();

    /**
     * @param string $idocType
     * @return void
     */
    public function setIdocType($idocType);

    /**
     * @return string
     */
    public function getMessageType();

    /**
     * @param string $messageType
     * @return void
     */
    public function setMessageType($messageType);

    /**
     * @return int
     */
    public function getStatus();

    /**
     * @return string
     */
    public function getSAPStatus();

    /**
     * @param int $status
     * @return void
     */
    public function setStatus($status);

    /**
     * @return string
     */
    public function getStatusText();

    /**
     * @param string $statusText
     * @return void
     */
    public function setStatusText($statusText);

    /**
     * @return int
     */
    public function getProcessingStatus();

    /**
     * @param int $processingStatus
     * @return void
     */
    public function setProcessingStatus($processingStatus);

}

?>