<?php

namespace Bilot\IdocReceiver\Api\Idoc;

use Magento\Framework\Exception\LocalizedException;

interface Idoc {

    /**
     * @param \SimpleXMLElement $xml
     * @return void
     */
    public function setup(\SimpleXMLElement $xml);

    /**
     * Persist IDOC to database
     * @return void
     * @throws LocalizedException
     */
    public function persist();

    /**
     * @return \Bilot\IdocReceiver\Model\Idoc\Metadata
     */
    public function getMetadata();

}