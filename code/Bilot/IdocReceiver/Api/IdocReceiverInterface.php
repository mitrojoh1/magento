<?php

namespace Bilot\IdocReceiver\Api;

/**
 * Interface for receiving Bilot SmartChannel IDOC messages
 *
 * @api
 */
interface IdocReceiverInterface
{

    /**
     * File name prefix for IDOC messages
     */
    const IDOC_FILE_PREFIX = "IDOC_";


    /**
     * IDOC receiver for MATMAS xml
     *
     * @param string $messageType
     * @return string Statistics
     * @throws \Magento\Framework\Exception\InputException
     */
    public function storeIDOC($messageType);
}

?>