<?php

namespace Bilot\IdocReceiver\Setup\Impl;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class BilotIdocStatus
{

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public static function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        /*
         * Prepare database for tables install
         */
        $installer->startSetup();

        /**
         * Create table 'bilot_idoc_status'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('bilot_idoc_status'))
            ->addColumn(
                'idoc_number',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                16,
                ['nullable' => false, 'primary' => true],
                'IDOC Number'
            )
            ->addColumn(
                'idoc_type',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                30,
                ['nullable' => true],
                'IDOC type'
            )
            ->addColumn(
                'message_type',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                30,
                ['nullable' => true],
                'Message type'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Status'
            )
            ->addColumn(
                'status_text',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Status text'
            )
            ->addColumn(
                'processing_status',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Status'
            )
            ->addIndex(
                $installer->getIdxName('idoc_ack_status', ['processing_status']),
                ['processing_status']
            )
            ->setComment('IDOC acknowledgement status Table');

        $installer->getConnection()->createTable($table);

        /*
         * Prepare database for tables install
         */
        $installer->endSetup();
    }

}

?>