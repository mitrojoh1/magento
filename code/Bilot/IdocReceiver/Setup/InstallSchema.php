<?php

namespace Bilot\IdocReceiver\Setup;

use Bilot\IdocReceiver\Setup\Impl\BilotIdocStatus;
use Bilot\IdocReceiver\Setup\Impl\InstallBilotIdocStatus;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @inheritDoc
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        BilotIdocStatus::install($setup, $context);
    }


}

?>