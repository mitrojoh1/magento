<?php

namespace Bilot\IdocReceiver\Cron;

use Bilot\IdocReceiver\Model\ResourceModel\IdocStatus;
use Bilot\IdocReceiver\Model\Util\Console;

/**
 * CronJob for triggering IDOC import.
 */
class IdocImportCronJob
{

    const FILE_PREFIX = "IDOC_";

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\Filesystem\Directory\ReadFactory
     */
    protected $readFactory;

    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteFactory $writeFactory
     */
    protected $writeFactory;

    /**
     * @var \Bilot\IdocReceiver\Model\IdocConfig
     */
    protected $config;

    /**
     * @var \Magento\Framework\Simplexml\Config
     */
    protected $xmlParser;

    /**
     * @var \Bilot\IdocReceiver\Model\Idoc\IdocTypeResolver
     */
    protected $idocResolver;

    /**
     * @param \Magento\Framework\Filesystem\Directory\ReadFactory $readFactory
     * @param \Magento\Framework\Filesystem\Directory\WriteFactory $writeFactory
     * @param \Bilot\IdocReceiver\Model\IdocConfig $config
     * @param \Magento\Framework\Simplexml\Config $parser
     * @param \Bilot\IdocReceiver\Model\Idoc\IdocTypeResolver $idocResolver
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\Filesystem\Directory\ReadFactory $readFactory,
        \Magento\Framework\Filesystem\Directory\WriteFactory $writeFactory,
        \Bilot\IdocReceiver\Model\IdocConfig $config,
        \Magento\Framework\Simplexml\Config $parser,
        \Bilot\IdocReceiver\Model\Idoc\IdocTypeResolver $idocResolver,
        \Psr\Log\LoggerInterface $logger)
    {

        $this->readFactory = $readFactory;
        $this->writeFactory = $writeFactory;
        $this->xmlParser = $parser;
        $this->config = $config;
        $this->idocResolver = $idocResolver;
        $this->logger = $logger;
    }

    /**
     * Execute IDOC import
     *
     * @return int Number of IDOCs processed
     */
    public function execute()
    {
        $read = $this->readFactory->create(
            $this->getIdocFolder(),
            \Magento\Framework\Filesystem\DriverPool::FILE);

        $write = $this->writeFactory->create(
            $this->getIdocFolder(),
            \Magento\Framework\Filesystem\DriverPool::FILE);

        // read IDOC directory content
        $files = $read->read();
        $count = 0;
        foreach ($files as $file) {
            if ($this->isIDOCFile($file)) {

                $this->logger->info("Processing file: " . $file . "...");
                Console::log("Processing file: " . $file . "...");

                // load XML IDOC
                $path = $this->getIdocFolder() . $file;
                if (!($xml = simplexml_load_file($path))) {
                    $this->logger->error("Failed to load XML from file: " + $path);
                    continue;
                }

                // process each IDOC
                $idocs = $this->findIdocsInFile($xml);
                foreach ($idocs as $idoc) {

                    $start = microtime(true);

                    $idocInstance = $this->idocResolver->resolve($idoc);
                    $idocInstance->persist();

                    $end = microtime(true);

                    $idocnumber = $idocInstance->getMetadata()->getIdocNumber();
                    $idoctype = $idocInstance->getMetadata()->getIdocType();
                    $exttype = $idocInstance->getMetadata()->getExtensionType();

                    $this->logger->info("IDOC: " . $idocnumber . ", type: " . $idoctype . "(" . $exttype . ") processed in" . round(($end - $start) * 1000) . " ms.");
                    Console::log("IDOC: " . $idocnumber . ", type: " . $idoctype . "(" . $exttype . ") processed in " . round(($end - $start) * 1000) . " ms.");

                    $count++;
                }

                // delete input file
                $write->delete($file);
            }
        }
        $this->logger->info("Finished importing IDOCs: " . $count . " message(s) processed.");
        Console::log("Finished importing IDOCs: " . $count . " message(s) processed.");

        return $count;
    }

    function isIDOCFile($file)
    {
        return (strpos($file, self::FILE_PREFIX) === 0);
    }

    /**
     * @return string IDOC folder
     */
    private function getIdocFolder()
    {
        return $this->config->getIdocFolder();
    }

    /**
     * @param \SimpleXMLElement $xml
     * @return \SimpleXMLElement[]
     */
    private function findIdocsInFile($xml)
    {
        return $xml->xpath("//*/IDOC");
    }

}

?>