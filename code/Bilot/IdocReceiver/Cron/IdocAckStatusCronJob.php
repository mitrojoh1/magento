<?php

namespace Bilot\IdocReceiver\Cron;


use Bilot\IdocReceiver\Model\ResourceModel\IdocStatus;
use Bilot\IdocReceiver\Model\Test\TestBean;
use Bilot\IdocReceiver\Model\Util\Console;

class IdocAckStatusCronJob
{

    /**
     * @var \Bilot\IdocReceiver\Model\Idoc\Status\IdocAckStatusBeanFactory
     */
    protected $statusBeanFactory;

    /**
     * @var \Bilot\IdocReceiver\Model\IdocStatusFactory
     */
    protected $idocStatusFactory;

    /**
     * @var \Bilot\RfcApi\Model\Service\RfcService
     */
    protected $rfcService;

    /**
     * @var \Bilot\IdocReceiver\Api\IdocStatusRepositoryInterface
     */
    protected $idocStatusRepository;

    /**
     * IdocAckStatusCronJob constructor.
     * @param \Bilot\IdocReceiver\Model\Idoc\Status\IdocAckStatusBeanFactory $statusBeanFactory
     * @param \Bilot\IdocReceiver\Model\IdocStatusFactory $idocStatusFactory
     * @param \Bilot\IdocReceiver\Api\IdocStatusRepositoryInterface $idocStatusRepository
     */
    public function __construct(
        \Bilot\IdocReceiver\Model\Idoc\Status\IdocAckStatusBeanFactory $statusBeanFactory,
        \Bilot\IdocReceiver\Model\IdocStatusFactory $idocStatusFactory,
        \Bilot\RfcApi\Model\Service\RfcService $rfcService,
        \Bilot\IdocReceiver\Api\IdocStatusRepositoryInterface $idocStatusRepository)
    {
        $this->statusBeanFactory = $statusBeanFactory;
        $this->idocStatusFactory = $idocStatusFactory;
        $this->rfcService = $rfcService;
        $this->idocStatusRepository = $idocStatusRepository;
    }

    public function execute()
    {
        $statusBean = $this->statusBeanFactory->create();

        $statuses = $this->idocStatusRepository->getNonProcessedEntries();
        if (!empty($statuses)) {
            // send status to ERP
            $statusBean->execute($this->rfcService, $statuses);

            // update processing status
            $this->idocStatusRepository->updateProcessingStatusToProcessed($statuses);
        }

        $this->logger->info("Finished updating IDOC import statuses to ERP.");
        Console::log("Finished updating IDOC import statuses to ERP.");
    }

}