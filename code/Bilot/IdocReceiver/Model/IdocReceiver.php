<?php

namespace Bilot\IdocReceiver\Model;

class IdocReceiver implements \Bilot\IdocReceiver\Api\IdocReceiverInterface
{

    /**
     * @var \Magento\Framework\Filesystem\File\WriteFactory
     */
    protected $writeFactory;

    /**
     * @var \Bilot\IdocReceiver\Model\IdocConfig
     */
    protected $config;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @param \Magento\Framework\Filesystem\File\WriteFactory $writeFactory
     * @param \Bilot\IdocReceiver\Model\IdocConfig $config
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\Filesystem\File\WriteFactory $writeFactory,
        \Bilot\IdocReceiver\Model\IdocConfig $config,
        \Psr\Log\LoggerInterface $logger)
    {

        $this->writeFactory = $writeFactory;
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function storeIDOC($messageType)
    {
        $payload = $this->getPayload();
        $filepath = $this->getFilePath($messageType);
        $file = $this->createFile($filepath);
        $file->write($payload);

        $this->logger->info("Storing IDOC " . $messageType . " to file: " . $filepath);

        return $filepath;
    }

    private function createFile($filepath)
    {
        return $this->writeFactory->create($filepath, \Magento\Framework\Filesystem\DriverPool::FILE, "w+");
    }

    private function getFilePath($messageType)
    {
        $folder = $this->getFolderFromConfig();
        $prefix = $this->getFilePrefix();
        return $folder . $prefix . $messageType . "-" . uniqid() . ".xml";
    }

    private function getFolderFromConfig()
    {
        return $this->config->getIdocFolder();
    }

    private function getPayload()
    {
        return file_get_contents('php://input');
    }

    private function getFilePrefix()
    {
        return \Bilot\IdocReceiver\Api\IdocReceiverInterface::IDOC_FILE_PREFIX;
    }

}

?>