<?php

namespace Bilot\IdocReceiver\Model\Util;

class ConversionTools {

    /**
     * Cut off leading zero's
     * @param string $input
     * @return string|null
     */
    public static function cutoffLeadingZeros($input) {
        return ltrim($input, '0');
    }

}

?>