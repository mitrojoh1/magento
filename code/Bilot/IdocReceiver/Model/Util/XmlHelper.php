<?php

namespace Bilot\IdocReceiver\Model\Util;

class XmlHelper {

    /**
     * @param \SimpleXMLElement[] $elements
     * @return string value
     */
    public static function getFirstElement(array $elements) {
        if (count($elements) > 0) {
            return $elements[0]->__toString();
        }
        return null;
    }

}

?>