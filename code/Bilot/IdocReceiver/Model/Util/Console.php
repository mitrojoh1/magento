<?php

namespace Bilot\IdocReceiver\Model\Util;

class Console {

    /**
     * @param string $message
     */
    public static function log($message) {
        echo $message . PHP_EOL;
    }

}