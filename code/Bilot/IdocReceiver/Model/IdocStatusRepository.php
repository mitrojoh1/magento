<?php

namespace Bilot\IdocReceiver\Model;


use Bilot\IdocReceiver\Api\Data\IdocStatusInterface;
use Bilot\IdocReceiver\Api\IdocStatusRepositoryInterface;
use Bilot\IdocReceiver\Model\ResourceModel\IdocStatus;
use Bilot\IdocReceiver\Model\Util\Console;

class IdocStatusRepository implements IdocStatusRepositoryInterface
{

    /**
     * @var ResourceModel\IdocStatus
     */
    protected $resourceModel;

    /**
     * @var \Bilot\IdocReceiver\Model\IdocStatusFactory
     */
    protected $idocStatusFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * IdocStatusRepository constructor.
     * @param ResourceModel\IdocStatus $idocStatus
     * @param IdocStatusFactory $idocStatusFactory
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     */
    public function __construct(
        \Bilot\IdocReceiver\Model\ResourceModel\IdocStatus $idocStatus,
        \Bilot\IdocReceiver\Model\IdocStatusFactory $idocStatusFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper) {

        $this->resourceModel = $idocStatus;
        $this->idocStatusFactory = $idocStatusFactory;
        $this->dataObjectHelper = $dataObjectHelper;
    }

    /**
     * @param IdocStatusInterface $idocStatus
     * @return IdocStatusInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Bilot\IdocReceiver\Api\Data\IdocStatusInterface $idocStatus)
    {
        $existing = $this->resourceModel->getIdByIdocNumber($idocStatus->getIdocNumber());
        if (!empty($existing)) {

            /** @var \Bilot\IdocReceiver\Model\IdocStatus $entity */
            $entity = $idocStatus;
            $entity->setId($existing);
        }

        return $this->resourceModel->save($idocStatus);
    }

    /**
     * @param \Bilot\IdocReceiver\Api\Data\IdocStatusInterface[] $statuses
     */
    public function updateProcessingStatusToProcessed($statuses) {
        foreach ($statuses as $status) {
            $status->setProcessingStatus(IdocStatus::PROCESSED);
            $this->resourceModel->save($status);
        }
    }

    /**
     * Get all non-processed entries
     * @return \Bilot\IdocReceiver\Api\Data\IdocStatusInterface[]
     */
    public function getNonProcessedEntries() {
        $return = array();
        $entries = $this->resourceModel->getNonProcessedEntries();
        foreach ($entries as $entry) {
            $status = $this->idocStatusFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $status,
                $entry,
                '\Bilot\IdocReceiver\Api\Data\IdocStatusInterface'
            );
            array_push($return, $status);
        }
        return $return;
    }
}