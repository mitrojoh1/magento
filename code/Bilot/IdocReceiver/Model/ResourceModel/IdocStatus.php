<?php

namespace Bilot\IdocReceiver\Model\ResourceModel;


use Bilot\IdocReceiver\Model\Util\Console;

class IdocStatus extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    const PROCESSED = 1;
    const NOT_PROCESSED = 0;

    const STATUS_OK = 1;
    const STATUS_NOT_OK = 99;

    /**
     * @var \Magento\Framework\EntityManager\EntityManager
     */
    protected $entityManager;

    /**
     * Class constructor
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param string $connectionName
     */
    public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context, $connectionName = null)
    {
        parent::__construct($context, $connectionName);
    }

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('bilot_idoc_status', 'idoc_number');
        $this->_isPkAutoIncrement = false; // no auto increment - we have custom primary key
    }

    /**
     * Fetch all non-processed entries
     * @return array
     */
    public function getNonProcessedEntries()
    {
        return $this->getEntriesByStatus(self::NOT_PROCESSED);
    }

    /**
     * Fetch all processed entries
     * @return array
     */
    public function getProcessedEntries()
    {
        return $this->getEntriesByStatus(self::PROCESSED);
    }

    /**
     * @param int $status
     * @return array
     */
    public function getEntriesByStatus($status)
    {
        $connection = $this->getConnection();
        $select = $connection->select()->from('bilot_idoc_status')->where('processing_status = :processingStatus');
        $bind = [':processingStatus' => $status];
        return $connection->fetchAll($select, $bind);
    }

    /**
     * @param $idocnumber
     * @return string idoc number
     */
    public function getIdByIdocNumber($idocnumber) {
        $connection = $this->getConnection();
        $select = $connection->select()->from('bilot_idoc_status', 'idoc_number')->where('idoc_number = :idocNumber');
        $bind = [':idocNumber' => $idocnumber];
        return $connection->fetchOne($select, $bind);
    }


}