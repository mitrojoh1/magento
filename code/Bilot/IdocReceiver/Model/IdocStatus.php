<?php

namespace Bilot\IdocReceiver\Model;

use Bilot\IdocReceiver\Api\Data\IdocStatusInterface;
use Bilot\IdocReceiver\Model\Util\Console;

class IdocStatus extends \Magento\Catalog\Model\AbstractModel implements IdocStatusInterface
{

    /**
     * IdocStatus constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Bilot\IdocReceiver\Model\ResourceModel\IdocStatus $resource,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [])
    {

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $storeManager,
            $resource,
            $resourceCollection,
            $data);

    }

    /**
     * @return string
     */
    public function getIdocNumber()
    {
        return $this->getData(IdocStatusInterface::IDOC_NUMBER);
    }

    /**
     * @param string $idocNumber
     * @return void
     */
    public function setIdocNumber($idocNumber)
    {
        $this->setData(IdocStatusInterface::IDOC_NUMBER, $idocNumber);
    }

    /**
     * @return string
     */
    public function getIdocType()
    {
        return $this->getData(IdocStatusInterface::IDOC_TYPE);
    }

    /**
     * @param string $idocType
     * @return void
     */
    public function setIdocType($idocType)
    {
        $this->setData(IdocStatusInterface::IDOC_TYPE, $idocType);
    }

    /**
     * @return string
     */
    public function getMessageType()
    {
        return $this->getData(IdocStatusInterface::MESSAGE_TYPE);
    }

    /**
     * @param string $messageType
     * @return void
     */
    public function setMessageType($messageType)
    {
        $this->setData(IdocStatusInterface::MESSAGE_TYPE, $messageType);
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->getData(IdocStatusInterface::STATUS);
    }

    /**
     * @return string
     */
    public function getSAPStatus()
    {
        return (\Bilot\IdocReceiver\Model\ResourceModel\IdocStatus::STATUS_OK == $this->getStatus() ? "I" : "E");
    }

    /**
     * @param tinyint $status
     * @return void
     */
    public function setStatus($status)
    {
        $this->setData(IdocStatusInterface::STATUS, $status);
    }

    /**
     * @return string
     */
    public function getStatusText()
    {
        return $this->getData(IdocStatusInterface::STATUS_TEXT);
    }

    /**
     * @param string $statusText
     * @return void
     */
    public function setStatusText($statusText)
    {
        $this->setData(IdocStatusInterface::STATUS_TEXT, $statusText);
    }

    /**
     * @return int
     */
    public function getProcessingStatus()
    {
        return $this->getData(IdocStatusInterface::PROCESSING_STATUS);
    }

    /**
     * @param int $processingStatus
     * @return void
     */
    public function setProcessingStatus($processingStatus)
    {
        $this->setData(IdocStatusInterface::PROCESSING_STATUS, $processingStatus);
    }

}

?>