<?php

namespace Bilot\IdocReceiver\Model\Test;


use Bilot\RfcApi\Model\RfcBeanBase;

class TestBean extends RfcBeanBase
{

    /**
     * @var Search
     * @bilotabapbackend [type=STRUCTURE, direction=IMPORTING, name=IS_SEARCH, structureClass=\Bilot\IdocReceiver\Model\Test\Search]
     */
    protected $search;

    /**
     * @var int
     * @bilotabapbackend [type=SIMPLE, direction=IMPORTING, name=IV_MAXHITS]
     */
    protected $maxhits;

    /**
     * @var Result
     * @bilotabapbackend [type=TABLE, direction=EXPORTING, name=ET_RESULT, structureClass=\Bilot\IdocReceiver\Model\Test\Result]
     */
    protected $result;

    /**
     * @var int
     * @bilotabapbackend [type=SIMPLE, direction=EXPORTING, name=EV_NUMHITS]
     */
    protected $numberOfHits;

    /**
     * @var SubTotal
     * @bilotabapbackend [type=TABLE, direction=EXPORTING, name=ET_SUBTOTALS, structureClass=\Bilot\IdocReceiver\Model\Test\SubTotal]
     */
    protected $subtotals;

    public function __construct()
    {
        $this->search = new Search();
        $this->search->setPartner("0000000001");
        $this->search->setPartnerFunction("AG");
        $this->search->setSalesOrganisation("0001");
        $this->search->setDistributionChannel("01");
        $this->search->setDivision("01");
        //$this->search->setDocumentType("TA");
        $this->search->setDocumentCategory("0");

        $this->maxhits = 5;
    }

    /**
     * @param \Bilot\RfcApi\Model\Service\RfcService $rfcService
     */
    function execute($rfcService) {
        $rfcService->execute($this);

        print_r($this);
    }

    /**
     * @return string
     */
    public function getFunctionModuleName()
    {
        return "/BILOT/SCA_SALESDOC_SEARCH";
    }


}

class Search {

    /**
     * @var string
     * @bilotabapattribute [field=PARTNER]
     */
    protected $partner;

    /**
     * @var string
     * @bilotabapattribute [field=PARTNER_FCT]
     */
    protected $partnerFunction;

    /**
     * @var string
     * @bilotabapattribute [field=SALES_ORG]
     */
    protected $salesOrganisation;

    /**
     * @var string
     * @bilotabapattribute [field=DISTR_CHAN]
     */
    protected $distributionChannel;

    /**
     * @var string
     * @bilotabapattribute [field=DIVISION]
     */
    protected $division;

    /**
     * @var string
     * @bilotabapattribute [field=DOC_TYPE]
     */
    protected $documentType;

    /**
     * @var string
     * @bilotabapattribute [field=DOCCATEGORY]
     */
    protected $documentCategory;

    /**
     * @return string
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param string $partner
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
    }

    /**
     * @return string
     */
    public function getPartnerFunction()
    {
        return $this->partnerFunction;
    }

    /**
     * @param string $partnerFunction
     */
    public function setPartnerFunction($partnerFunction)
    {
        $this->partnerFunction = $partnerFunction;
    }

    /**
     * @return string
     */
    public function getSalesOrganisation()
    {
        return $this->salesOrganisation;
    }

    /**
     * @param string $salesOrganisation
     */
    public function setSalesOrganisation($salesOrganisation)
    {
        $this->salesOrganisation = $salesOrganisation;
    }

    /**
     * @return string
     */
    public function getDistributionChannel()
    {
        return $this->distributionChannel;
    }

    /**
     * @param string $distributionChannel
     */
    public function setDistributionChannel($distributionChannel)
    {
        $this->distributionChannel = $distributionChannel;
    }

    /**
     * @return string
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * @param string $division
     */
    public function setDivision($division)
    {
        $this->division = $division;
    }

    /**
     * @return string
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * @param string $documentType
     */
    public function setDocumentType($documentType)
    {
        $this->documentType = $documentType;
    }

    /**
     * @return string
     */
    public function getDocumentCategory()
    {
        return $this->documentCategory;
    }

    /**
     * @param string $documentCategory
     */
    public function setDocumentCategory($documentCategory)
    {
        $this->documentCategory = $documentCategory;
    }


}

class Result {

    /**
     * @var string
     * @bilotabapattribute [field=VBELN]
     */
    protected $document;

    /**
     * @return string
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param string $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

}

class SubTotal {

    /**
     * @var string
     * @bilotabapattribute [field=SD_DOC]
     */
    protected $documentNumber;

    /**
     * @var string
     * @bilotabapattribute [field=ITM_NUMBER]
     */
    protected $itemNumber;

    /**
     * @return string
     */
    public function getDocumentNumber()
    {
        return $this->documentNumber;
    }

    /**
     * @param string $documentNumber
     */
    public function setDocumentNumber($documentNumber)
    {
        $this->documentNumber = $documentNumber;
    }

    /**
     * @return string
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * @param string $itemNumber
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;
    }

}