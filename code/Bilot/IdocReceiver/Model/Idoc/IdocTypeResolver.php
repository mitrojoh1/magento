<?php

namespace Bilot\IdocReceiver\Model\Idoc;

use Bilot\IdocReceiver\Model\Util\Console;
use Magento\Framework\Exception\NotFoundException;

class IdocTypeResolver
{

    const MATMAS = "\\Bilot\\IdocReceiver\\Model\\Idoc\\Matmas\\IdocMatmas";

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;


    /**
     * IdocTypeResolver constructor.
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param \SimpleXMLElement $xml
     * @return \Bilot\IdocReceiver\Api\Idoc\Idoc
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function resolve(\SimpleXMLElement $xml)
    {
        // identify IDOC type
        $metadata = new \Bilot\IdocReceiver\Model\Idoc\Metadata($xml);

        /**
         * @var \Bilot\IdocReceiver\Model\Idoc\IdocBase $idocInstance
         */
        $idocInstance = null;

        if ($metadata->isMATMAS()) {
            $idocInstance =  $this->objectManager->create(self::MATMAS);
        } else {
            throw new NotFoundException();
        }

        if ($idocInstance != null) {
            $idocInstance->setMetadata($metadata);
            $idocInstance->setup($xml);
        }

        return $idocInstance;
    }

}