<?php

namespace Bilot\IdocReceiver\Model\Idoc;

use Bilot\IdocReceiver\Model\Util\Console;

/**
 * Class representing and reading IDOC metadata
 */
class Metadata extends IdocSegmentBase
{

    const METADATA_SEGMENT = "EDI_DC40";

    const IDOCTYPE = "IDOCTYP";
    const CIMTYPE = "CIMTYP";
    const MESTYPE = "MESTYP";
    const DOCUMENT = "DOCNUM";

    /**
     * IDOC type contants
     */
    const MATMAS = "MATMAS";
    const BILOT_MATMAS = "/BILOT/MATMAS";
    const DEBMAS = "DEBMAS";
    const BILOT_DEBMAS = "/BILOT/DEBMAS";

    /**
     * @param \SimpleXMLElement $element
     */
    public function __construct(\SimpleXMLElement $element)
    {
        parent::__construct($element->xpath(self::METADATA_SEGMENT)[0]);
    }

    /**
     * @return string IDOC message type
     */
    public function getMessageType()
    {
        return $this->getValueByXPath(self::MESTYPE);
    }

    /**
     * @return string IDOC type
     */
    public function getIdocType()
    {
        return $this->getValueByXPath(self::IDOCTYPE);
    }

    /**
     * @return string IDOC extension type
     */
    public function getExtensionType()
    {
        return $this->getValueByXPath(self::CIMTYPE);
    }

    /**
     * @return string IDOC number
     */
    public function getIdocNumber()
    {
        return $this->getValueByXPath(self::DOCUMENT);
    }

    /**
     * Check whether idoc is of type MATMAS
     * @return bool true if MATMAS message, otrherwise false
     */
    public function isMATMAS()
    {
        $mestype = $this->getMessageType();
        return ($mestype === self::BILOT_MATMAS || $mestype === self::MATMAS);
    }

    /**
     * Check whether idoc is of type DEBMAS
     * @return bool true if DEBMAS message, otrherwise false
     */
    public function isDEBMAS()
    {
        $mestype = $this->getMessageType();
        return ($mestype === self::BILOT_DEBMAS || $mestype === self::DEBMAS);
    }

}

?>