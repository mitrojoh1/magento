<?php

namespace Bilot\IdocReceiver\Model\Idoc\Matmas;

use Bilot\IdocReceiver\Model\Idoc\IdocSegmentBase;
use Bilot\IdocReceiver\Model\Util\ConversionTools;


class MatmasMaram extends IdocSegmentBase
{

    /**
     * IDOC segment name
     */
    const IDOC_SEGMENT = "E1MARAM";

    const MATERIAL = "MATNR";
    const WEIGHT = "NTGEW";

    /**
     * MatmasMaram constructor.
     * @param \SimpleXMLElement $element
     */
    public function __construct(\SimpleXMLElement $element)
    {
        parent::__construct($element->xpath(self::IDOC_SEGMENT)[0]);
    }

    public function getMaterialCode()
    {
        return ConversionTools::cutoffLeadingZeros($this->getValueByXPath(self::MATERIAL));
    }

    public function getWeight()
    {
        return $this->getValueByXPath(self::WEIGHT);
    }
}

?>