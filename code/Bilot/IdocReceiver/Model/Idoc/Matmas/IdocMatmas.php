<?php

namespace Bilot\IdocReceiver\Model\Idoc\Matmas;

use Bilot\IdocReceiver\Model\Idoc\IdocBase;
use Bilot\IdocReceiver\Model\ResourceModel\IdocStatus;
use Bilot\IdocReceiver\Model\Util\Console;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Exception\NoSuchEntityException;


class IdocMatmas extends IdocBase
{

    const TYPE_SIMPLE = "simple";

    const ATTRIBUTE_SET_BILOT = "Bilot";


    /**
     * @var \Bilot\IdocReceiver\Model\Idoc\Matmas\MatmasMaram
     */
    protected $maram;

    /**
     * @var \Bilot\IdocReceiver\Model\Idoc\Matmas\MatmasMaktm
     */
    protected $maktm;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;


    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $productResource;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Catalog\Api\AttributeSetRepositoryInterface
     */
    protected $attributeSetRepository;

    /**
     * @var \Magento\Framework\Api\Search\SearchCriteriaFactory
     */
    protected $searchCriteriaFactory;

    /**
     * @var \Magento\Eav\Api\Data\AttributeSetInterface
     */
    protected $attributeSet;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    /**
     * IdocMatmas constructor.
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product $productResource
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository ,
     * @param \Magento\Catalog\Api\AttributeSetRepositoryInterface $attributeSetRepository ,
     * @param \Magento\Framework\Api\Search\SearchCriteriaFactory $searchCriteriaFactory ,
     * @param \Magento\Framework\EntityManager\TypeResolver $typeResolver
     */
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product $productResource,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Api\AttributeSetRepositoryInterface $attributeSetRepository,
        \Magento\Framework\Api\Search\SearchCriteriaFactory $searchCriteriaFactory,
        \Magento\Eav\Model\Config $eavConfig)
    {
        $this->productFactory = $productFactory;
        $this->productResource = $productResource;
        $this->productRepository = $productRepository;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->searchCriteriaFactory = $searchCriteriaFactory;
        $this->eavConfig = $eavConfig;
    }


    /**
     * @inheritDoc
     */
    public function setup(\SimpleXMLElement $xml)
    {
        $this->maram = new MatmasMaram($xml);
        $this->maktm = new MatmasMaktm($xml);
    }

    /**
     * @inheritDoc
     */
    public function persist()
    {
        try {
            $product = $this->getExistingProduct();
            if (is_null($product)) {

                Console::log("Creating product: " . $this->maram->getMaterialCode());
                $this->getLogger()->info("Creating product: " . $this->maram->getMaterialCode());

                $product = $this->productFactory->create();
                $product->setSku($this->maram->getMaterialCode());
                $this->applyAttributeSet($product);
                $this->populateAttributes($product);
            } else {

                Console::log("Modifying existing product: " . $this->maram->getMaterialCode());
                $this->getLogger()->info("Modifying existing product: " . $this->maram->getMaterialCode());

                $this->populateAttributes($product);
            }

            Console::log("Saving...");
            $this->getLogger()->info("Saving...");

            $this->productResource->save($product);

            Console::log("Product saved: " . $this->maram->getMaterialCode());
            $this->getLogger()->info("Product saved: " . $this->maram->getMaterialCode());

            // persist status in DB
            $this->persistStatus(IdocStatus::STATUS_OK, "Successfully imported");

        } catch (\Exception $ex) {
            // persist status in DB
            $this->persistStatus(IdocStatus::STATUS_NOT_OK, is_null($ex->getMessage()) ? "Unknown error" : $ex->getMessage());
        }
    }

    /**
     * @param ProductInterface $product
     */
    private function applyAttributeSet($product)
    {
        if ($this->attributeSet == null) {
            $this->attributeSet = $this->getOrCreateDefaultAttributeSet();
        }
        $product->setAttributeSetId($this->attributeSet->getAttributeSetId());
    }


    /**
     * @return ProductInterface|null
     */
    private function getExistingProduct()
    {
        $product = null;
        try {
            $product = $this->productRepository->get($this->maram->getMaterialCode(), true);
        } catch (NoSuchEntityException $ex) {
        }
        return $product;
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     */
    protected function populateAttributes($product)
    {
        $product->setName($this->maktm->getName("EN"));
        $product->getWeight($this->maram->getWeight());
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return \Magento\Eav\Api\Data\AttributeSetInterface
     */
    private function getOrCreateDefaultAttributeSet()
    {
        $attributeSet = $this->getAttributeSet();
        if ($attributeSet == null) {
            $attributeSet = $this->createAttributeSet();
            $attributeSet->setEntityTypeId($this->getEntityType());
            $attributeSet->setAttributeSetName(self::ATTRIBUTE_SET_BILOT);
            $attributeSet = $this->attributeSetRepository->save($attributeSet);
        }
        return $attributeSet;
    }

    /**
     * @return int
     */
    private function getEntityType()
    {
        return $this->eavConfig->getEntityType(\Magento\Catalog\Model\Product::ENTITY)->getId();
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return \Magento\Eav\Api\Data\AttributeSetInterface
     */
    private function createAttributeSet()
    {
        return $this->getObjectManager()->get('Magento\Eav\Api\Data\AttributeSetInterface');
    }

    /**
     * @return \Magento\Eav\Api\Data\AttributeSetInterface|null
     */
    private function getAttributeSet()
    {
        $result = null;
        $searchCriteria = $this->searchCriteriaFactory->create();
        $attributeSets = $this->attributeSetRepository->getList($searchCriteria)->getItems();
        foreach ($attributeSets as $attributeSet) {
            if (self::ATTRIBUTE_SET_BILOT === $attributeSet->getAttributeSetName()) {
                $result = $attributeSet;
                break;
            }
        }
        return $result;
    }
}

?>