<?php

namespace Bilot\IdocReceiver\Model\Idoc\Matmas;

use Bilot\IdocReceiver\Model\Idoc\IdocLanguageAwareBase;


/**
 * Class MatmasMaktm
 *
 * <E1MAKTM SEGMENT="1">
 *   <SPRAS>E</SPRAS>
 *   <MAKTX>AVOCADO HASS READY TO EAT</MAKTX>
 *   <SPRAS_ISO>EN</SPRAS_ISO>
 * </E1MAKTM>
 *
 * @package Bilot\IdocReceiver\Model\Idoc\Matmas
 */
class MatmasMaktm extends IdocLanguageAwareBase
{
    /**
     * IDOC segment name
     */
    const IDOC_SEGMENT = "E1MARAM/E1MAKTM";

    const DESCRIPTION = "MAKTX";

    /**
     * @var array {
     *      key => Language ISO code (upper case)
     *      value = \SimpleXMLElement
     * }
     */
    protected $elements;

    /**
     * @param \SimpleXMLElement $element
     */
    public function __construct(\SimpleXMLElement $element)
    {
        parent::__construct($element->xpath(self::IDOC_SEGMENT), "SPRAS_ISO");
    }

    /***
     * Read language specific product name
     *
     * @param string $language (iso code)
     * @return string product name
     */
    public function getName($language)
    {
        return $this->getValueByXpathWithLang($language, self::DESCRIPTION);
    }

}

?>