<?php
/**
 * Created by PhpStorm.
 * User: mitrojoh1
 * Date: 16.11.2016
 * Time: 20:52
 */

namespace Bilot\IdocReceiver\Model\Idoc\Status;


use Bilot\IdocReceiver\Api\Data\IdocStatusInterface;
use Bilot\RfcApi\Model\RfcBeanBase;

class IdocAckStatusBean extends RfcBeanBase
{

    /**
     * @var array
     * @bilotabapbackend [type=TABLE, direction=IMPORTING, name=IT_STATUSES, structureClass=\Bilot\IdocReceiver\Model\Idoc\Status\StatusEntry]
     */
    protected $statuses;

    /**
     * IdocAckStatusBean constructor.
     * @param \Bilot\RfcApi\Model\Service\RfcService $rfcService
     */
    public function __construct(\Bilot\RfcApi\Model\Service\RfcService $rfcService)
    {
        parent::__construct($rfcService);
    }

    /**
     * @param \Bilot\RfcApi\Model\Service\RfcService $rfcService
     * @param IdocStatusInterface[] $statuses
     */
    function execute($rfcService, $statuses)
    {
        $this->statuses = array();

        foreach ($statuses as $status) {
            array_push(
                $this->statuses,
                new StatusEntry(
                    $status->getIdocNumber(),
                    $status->getSAPStatus(),
                    $status->getStatusText())
            );
        }

        $rfcService->execute($this);
    }


    /**
     * @return string
     */
    public function getFunctionModuleName()
    {
        return "/BILOT/SCA_SET_IDOC_STATUS_MUL";
    }


}

class StatusEntry
{

    /**
     * @var string
     * @bilotabapattribute [field=IDOC_NUMBER]
     */
    private $idocNumber;

    /**
     * @var string
     * @bilotabapattribute [field=STATUS]
     */
    private $status;

    /**
     * @var string
     * @bilotabapattribute [field=STATUSMSG]
     */
    private $message;

    /**
     * @var string
     * @bilotabapattribute [field=STATUSMSG_LONG]
     */
    private $messageLong;

    /**
     * StatusEntry constructor.
     * @param string $idocNumber
     * @param string $status
     * @param string $statusmsg
     */
    public function __construct($idocNumber, $status, $statusmsg)
    {
        $this->idocNumber = $idocNumber;
        $this->status = $status;
        $this->message = $statusmsg;
        $this->messageLong = $statusmsg;
    }

    /**
     * @return string
     */
    public function getIdocNumber()
    {
        return $this->idocNumber;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getMessageLong() {
        return $this->messageLong;
    }

}