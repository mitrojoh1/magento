<?php

namespace Bilot\IdocReceiver\Model\Idoc;

use Bilot\IdocReceiver\Api\Idoc\Idoc;
use Bilot\IdocReceiver\Model\IdocStatus;

abstract class IdocBase implements Idoc  {

    /**
     * @var \Bilot\IdocReceiver\Model\Idoc\Metadata
     */
    protected $metadata;

    /**
     * @return Metadata
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param Metadata $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * Prepare data for entity creation
     * @param \SimpleXMLElement $xml
     * @return mixed
     */
    public abstract function setup(\SimpleXMLElement $xml);

    /**
     * @param int $status
     * @param string $statusText
     * @return \Bilot\IdocReceiver\Model\IdocStatus
     */
    protected function persistStatus($status, $statusText) {
        $idocStatus = $this->getIdocStatusFactory()->create();

        $idocStatus->setIdocNumber($this->getMetadata()->getIdocNumber());
        $idocStatus->setIdocType($this->getMetadata()->getIdocType());
        $idocStatus->setMessageType($this->getMetadata()->getMessageType());
        $idocStatus->setStatus($status);
        $idocStatus->setStatusText($statusText);
        $idocStatus->setProcessingStatus(\Bilot\IdocReceiver\Model\ResourceModel\IdocStatus::NOT_PROCESSED);

        $idocStatus = $this->getIdocStatusRepository()->save($idocStatus);

        return $idocStatus;
    }

    /**
     * @return \Bilot\IdocReceiver\Model\IdocStatusFactory
     */
    protected function getIdocStatusFactory() {
        return $this->getObjectManager()->get('\Bilot\IdocReceiver\Model\IdocStatusFactory');
    }

    /**
     * @return \Bilot\IdocReceiver\Api\IdocStatusRepositoryInterface
     */
    protected function getIdocStatusRepository() {
        return $this->getObjectManager()->get('\Bilot\IdocReceiver\Api\IdocStatusRepositoryInterface');
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    protected function getLogger() {
        return $this->getObjectManager()->get('\Psr\Log\LoggerInterface');
    }

    /**
     * @return \Magento\Framework\App\ObjectManager
     */
    protected function getObjectManager() {
        return \Magento\Framework\App\ObjectManager::getInstance();
    }

}

?>