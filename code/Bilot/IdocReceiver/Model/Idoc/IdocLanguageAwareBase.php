<?php

namespace Bilot\IdocReceiver\Model\Idoc;

use Bilot\IdocReceiver\Model\Util\XmlHelper;

class IdocLanguageAwareBase extends IdocSegmentBase  {

    /**
     * @var array {
     *      key => Language ISO code (upper case)
     *      value = \SimpleXMLElement
     * }
     */
    protected $elements;

    /**
     * IdocLanguageAwareBase constructor.
     * @param \SimpleXMLElement[] $elements
     * @param $xpath
     */
    public function __construct(array $elements, $xpath)
    {
        $this->elements = array();
        foreach ($elements as $element) {
            $matches = $element->xpath($xpath);
            $key = XmlHelper::getFirstElement($matches);
            if (!is_null($key)) {
                $this->elements[$key] = $element;
            }
        }
    }

    /**
     * @param string $language
     * @param string $xpath
     * @return string|null value
     */
    protected function getValueByXpathWithLang($language, $xpath) {
        $value = null;
        $element = $this->getElement($language);
        if ($element != null) {
            $values = $element->xpath($xpath);
            $value = XmlHelper::getFirstElement($values);
        }
        return $value;
    }

    /**
     * @param string $language
     * @return \SimpleXMLElement element
     */
    protected function getElement($language) {
        return $this->elements[$language];
    }

}

?>