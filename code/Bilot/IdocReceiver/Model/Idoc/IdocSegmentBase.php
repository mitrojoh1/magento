<?php

namespace Bilot\IdocReceiver\Model\Idoc;
use Bilot\IdocReceiver\Model\Util\Console;
use Bilot\IdocReceiver\Model\Util\XmlHelper;

/**
 * Class providing common functionality for IDOC data objects
 */
class IdocSegmentBase
{

    /**
     * @var \SimpleXMLElement
     */
    private $element;

    /**
     * @param \SimpleXMLElement $element
     */
    public function __construct(\SimpleXMLElement $element)
    {
        $this->element = $element;
    }

    /**
     * @param string $xpath
     * @return string value of element
     */
    protected function getValueByXPath($xpath) {
        $matches = $this->element->xpath($xpath);
        return XmlHelper::getFirstElement($matches);
    }

}

?>