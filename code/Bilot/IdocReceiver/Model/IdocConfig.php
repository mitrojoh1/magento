<?php

namespace Bilot\IdocReceiver\Model;

class IdocConfig
{

    /**
     * Configuration path: IDOC folder
     */
    const IDOC_FOLDER_CONFIG = "idocreceiver/general/folder";

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get IDOC folder
     *
     * @return string IDOC folder
     */
    public function getIdocFolder()
    {
        return $this->scopeConfig->getValue(self::IDOC_FOLDER_CONFIG);
    }

}

?>