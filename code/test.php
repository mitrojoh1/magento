<?php

class Bar {

    private $attr = 0;

}

class Foo
{

    public function __construct()
    {
        $this->attr = new Bar();
        $this->test = "test";
    }

    /**
     * @var mixed
     * @bilotabapbackend [type=TABLES, name=IT_TEST]
     */
    protected $test;

    protected $attr;

}

$foo = new Foo();

$reflection = new ReflectionClass($foo);
$properties = $reflection->getProperties();

$pattern = "#@[bilotabapbackend]+\s*\[([(a-zA-Z0-9)].*)\]#";

foreach ($properties as $property) {

    //preg_match_all($pattern, $property->getDocComment(), $annotation, PREG_PATTERN_ORDER);
    //echo count($annotation);

    $property->setAccessible(true);
    $val = $property->getValue($foo);
    if ($val != null && is_object($val)) {
        print_r($val);
    } else if ($val != null) {
        echo $val . PHP_EOL;
        $property->setValue($foo, "foo");
        echo $property->getValue($foo) . PHP_EOL;
    }


}

?>